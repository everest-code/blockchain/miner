package main

import (
	"crypto"
	"encoding/hex"
	"encoding/json"
	"hash"
	"strings"
	"time"

	"github.com/google/uuid"
)

type (
	HashFunc func([]byte) string

	MirrorResponse struct {
		Ok      bool        `json:"ok"`
		Payload interface{} `json:"payload"`
		Error   *string     `json:"error"`
	}

	AddRequest struct {
		Miner string                 `json:"miner"`
		Block map[string]interface{} `json:"block"`
	}

	BlockchainConfig struct {
		Difficulty    int     `json:"difficulty"`
		RepeatChar    string  `json:"repeat_char"`
		Tolerance     float32 `json:"tolerance"`
		AlgorythmName string  `json:"hash_algo"`
	}

	Block struct {
		ID           *string   `json:"id"`
		MetaID       uuid.UUID `json:"meta_id"`
		Content      string    `json:"content"`
		Parent       *string   `json:"parent"`
		Verfication  *int      `json:"verify_token"`
		CreationDate string    `json:"creation_date"`
	}
)

func (conf *BlockchainConfig) GetPrefix() string {
	return strings.Repeat(conf.RepeatChar, conf.Difficulty)
}

func (conf *BlockchainConfig) GetTolerance() time.Duration {
	return time.Duration(conf.Tolerance) * time.Second
}

func (conf *BlockchainConfig) GetAlgorythm() HashFunc {
	var hashGenerator func() hash.Hash

	switch conf.AlgorythmName {
	case "md5":
		hashGenerator = crypto.MD5.New
	case "sha1":
		hashGenerator = crypto.SHA1.New
	case "sha256":
		hashGenerator = crypto.SHA256.New
	case "sha384":
		hashGenerator = crypto.SHA384.New
	case "sha512":
		hashGenerator = crypto.SHA512.New
	}

	return func(raw []byte) string {
		h := hashGenerator()
		h.Write(raw)
		return hex.EncodeToString(h.Sum(nil))
	}
}

func (b *Block) GetMap() (res map[string]interface{}) {
	res = make(map[string]interface{})
	if b.ID == nil || *b.ID == "" {
		res["id"] = nil
	} else {
		res["id"] = *b.ID
	}

	res["meta_id"] = b.MetaID.String()
	res["content"] = b.Content
	if b.Parent == nil || *b.Parent == "" {
		res["parent"] = nil
	} else {
		res["parent"] = *b.Parent
	}
	res["creation_date"] = b.CreationDate
	if b.Verfication == nil || *b.Verfication == 0 {
		res["verify_token"] = nil
	} else {
		res["verify_token"] = *b.Verfication
	}
	return res
}

func (b *Block) GetJSON() []byte {
	res, _ := json.Marshal(b.GetMap())
	return res
}

func (b *Block) Validate(fx HashFunc, prefix string) bool {
	hashCode := fx(b.GetJSON())
	res := strings.HasPrefix(hashCode, prefix)
	if res {
		b.ID = &hashCode
	}
	return res
}

func (api *MirrorResponse) MarshalPayload(i interface{}) error {
	data, err := json.Marshal(api.Payload)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, i)
}
