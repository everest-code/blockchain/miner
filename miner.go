package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

type Miner struct {
	Block         *Block
	Config        *BlockchainConfig
	MaxThreads    int
	resolveChan   chan int
	actualThreads int
}

const MaxInt = int(^uint(0) >> 1)

func NewMiner(maxThreads int) *Miner {
	return &Miner{
		Block:         nil,
		Config:        nil,
		MaxThreads:    maxThreads,
		resolveChan:   make(chan int),
		actualThreads: 0,
	}
}

func (m *Miner) mine(pos int) {
	times := 0
	for true {
		if m.Block == nil {
			logger.Debug("Lazy wait start on %d thread", pos)
			times = 0
			time.Sleep(10 * time.Second)
			continue
		}

		var fracPos int = MaxInt / m.actualThreads
		min, max := fracPos*(pos-1), fracPos*pos

		block := *m.Block

		num := int(rand.Intn(max-min) + min)
		block.Verfication = &num
		if block.Validate(m.Config.GetAlgorythm(), m.Config.GetPrefix()) {
			m.resolveChan <- num
		}

		times++
		if times > 1e6 {
			logger.Debug("%d times mining on %s miner %d", int(1e6), block.MetaID, pos)
			times = 0
		}
	}
}

func (m *Miner) startThread() bool {
	if m.actualThreads < m.MaxThreads {
		m.actualThreads++
		go m.mine(m.actualThreads)
		logger.Debug("Thread %d was created", m.actualThreads)
		return true
	}
	return false
}

func (m *Miner) awaitCode() {
	for true {
		verifyToken := <-m.resolveChan
		block := *m.Block
		block.Verfication = &verifyToken
		logger.Info("Verify token for %s was discovered: %d", block.MetaID, verifyToken)
		if block.Validate(m.Config.GetAlgorythm(), m.Config.GetPrefix()) {
			logger.Info("Verify token was verified")
			if m.addToBlockchain(verifyToken) {
				m.deleteBlock()
				if err := m.RequestBlock(); err != nil {
					logger.Panic(err.Error())
				}
			} else {
				logger.Error("Unlocking block becuase an error due")
				ToggleBlockStatus(m.Block.MetaID)
				m.Block = nil
			}
		} else {
			logger.Warn("Verify token was false, still mining")
		}
	}
}

func (m *Miner) RequestBlock() error {
	logger.Info("Updating configuration")
	if conf, err := GetBlockchainConfig(); err != nil {
		return err
	} else {
		m.Config = conf
	}

	for m.Block == nil {
		block, err := GetLastBlock()
		if err != nil && !errors.Is(err, NoBlock) {
			return err
		} else if errors.Is(err, NoBlock) {
			logger.Info("Waiting for a block")
			time.Sleep(5 * time.Second)
			continue
		}

		m.Block = block
		ToggleBlockStatus(m.Block.MetaID)
		logger.Info("Block %s got", m.Block.MetaID)
	}

	return nil
}

func (m *Miner) addToBlockchain(token int) bool {
	block := *m.Block
	block.Verfication = &token
	if err := AddBlock(AddRequest{
		Miner: fmt.Sprintf("http://%s", address),
		Block: block.GetMap(),
	}); err != nil {
		logger.Error("Block has an error: %s", err.Error())
		return false
	}

	logger.Info("Block was %s accepted", block.MetaID)
	return true
}

func (m *Miner) deleteBlock() {
	if m.Block != nil {
		logger.Warn("Removing block %s", m.Block.MetaID)
		DeleteBlock(m.Block.MetaID)
		m.Block = nil
	}
}

func (m *Miner) Start() bool {
	if m.Block == nil || m.Config == nil {
		return false
	}

	for m.startThread() {
	}
	go m.awaitCode()

	return true
}
