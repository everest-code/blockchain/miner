package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/everest-code/tree-db/server/log"
)

var (
	logger  *log.Logger
	miner   *Miner
	mirror  string
	address string
)

func getEnvOrDefault(key, def string) (res string) {
	res = os.Getenv(key)
	if res == "" {
		res = def
	}
	return
}

func getIntEnvOrDefault(key string, def int) (res int) {
	res, _ = strconv.Atoi(getEnvOrDefault(key, fmt.Sprintf("%d", def)))
	return
}

func createServer(address string) *http.Server {
	router := mux.NewRouter()
	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			next.ServeHTTP(w, r)
			diff := time.Since(start)
			logger.Info("%s %s - %s %s", r.Method, r.URL, r.RemoteAddr, diff.String())
		})
	})

	router.HandleFunc("/update", func(w http.ResponseWriter, r *http.Request) {
		id := new(string)
		body, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal(body, id)

		if miner.Block != nil {
			if *id == miner.Block.MetaID.String() {
				go func() {
					miner.Block = nil
					miner.RequestBlock()
				}()
			}
		}

		w.WriteHeader(http.StatusAccepted)
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{\"ok\":true}"))
	}).Methods("POST")

	server := http.Server{
		Handler:      router,
		Addr:         address,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}
	logger.ImportantInfo("Server listening on: %s", address)
	return &server
}

func main() {
	logger, _ = log.NewConsole(log.LevelDebg, false)
	mirror = getEnvOrDefault("MIRROR", "<nil>")
	address = getEnvOrDefault("ADDR", "0.0.0.0:9001")

	if mirror == "<nil>" {
		log.Panic("Mirror was not set.")
	}

	miner = NewMiner(getIntEnvOrDefault("THREADS", runtime.NumCPU()))
	SyncInMirror()

	if err := miner.RequestBlock(); err != nil {
		log.Panic(err.Error())
	}

	miner.Start()

	server := createServer(address)
	err := server.ListenAndServe()
	if err != nil {
		log.Panic(err.Error())
	}
}
