package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/google/uuid"
)

var NoBlock error = errors.New("No block yet")

func makeGet(url string) (*MirrorResponse, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	result := new(MirrorResponse)
	if err := json.Unmarshal(data, result); err != nil {
		return nil, err
	}

	return result, nil
}

func makePost(url string, v interface{}) (*MirrorResponse, error) {
	body, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	res, err := http.Post(url, "application/json", strings.NewReader(string(body)))
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	result := new(MirrorResponse)
	if err := json.Unmarshal(data, result); err != nil {
		logger.Debug("Raw response is: %s", data)
		return nil, err
	}

	return result, nil
}

func makePut(url string, v interface{}) (*MirrorResponse, error) {
	client := &http.Client{}
	body, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	req, _ := http.NewRequest(http.MethodPut, url, strings.NewReader(string(body)))
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	result := new(MirrorResponse)
	if err := json.Unmarshal(data, result); err != nil {
		logger.Debug("Raw response is: %s", data)
		return nil, err
	}

	return result, nil
}

func makeDelete(url string, v interface{}) (*MirrorResponse, error) {
	client := &http.Client{}
	body, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	req, _ := http.NewRequest(http.MethodDelete, url, strings.NewReader(string(body)))
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	result := new(MirrorResponse)
	if err := json.Unmarshal(data, result); err != nil {
		logger.Debug("Raw response is: %s", data)
		return nil, err
	}

	return result, nil
}

func GetBlockchainConfig() (*BlockchainConfig, error) {
	res, err := makeGet(fmt.Sprintf("%s/blockchain/config", mirror))
	if err != nil {
		return nil, err
	}

	result := new(BlockchainConfig)
	if err := res.MarshalPayload(result); err != nil {
		return nil, err
	}
	return result, nil
}

func GetLastBlock() (*Block, error) {
	res, err := makeGet(fmt.Sprintf("%s/waitlist/last", mirror))
	if err != nil {
		return nil, err
	}

	if !res.Ok {
		return nil, NoBlock
	}

	result := new(Block)
	if err := res.MarshalPayload(result); err != nil {
		return nil, err
	}
	return result, nil
}

func ToggleBlockStatus(id uuid.UUID) bool {
	res, err := makePut(fmt.Sprintf("%s/waitlist/%s", mirror, id), nil)
	if err != nil {
		return false
	}

	return res.Ok
}

func DeleteBlock(id uuid.UUID) bool {
	res, err := makeDelete(fmt.Sprintf("%s/waitlist/%s", mirror, id), nil)
	if err != nil {
		return false
	}

	return res.Ok
}

func AddBlock(request AddRequest) error {
	res, err := makePost(fmt.Sprintf("%s/blockchain/add", mirror), request)
	if err != nil {
		return err
	}

	if !res.Ok {
		logger.Warn("JSON sent: %#v", request.Block)
		return errors.New(*res.Error)
	}
	return nil
}

func SyncInMirror() error {
	res, err := makePost(fmt.Sprintf("%s/sync/miners", mirror), fmt.Sprintf("http://%s", address))
	if err != nil {
		return err
	}

	if !res.Ok {
		return errors.New(*res.Error)
	}
	return nil
}
