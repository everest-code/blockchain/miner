module gitlab.com/everest-code/blockchain/miner

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	gitlab.com/everest-code/tree-db/server v0.1.1
)

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20211210111614-af8b64212486 // indirect
)
